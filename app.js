var express = require('express');
var app = express();
const createError = require('http-errors')
const store = new ( require('simple-memory-store'))();
var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
var httpErr =require('http-error')


const dependencies = {
    users: { tweets: "tweets" },
    tweets: { users: "user" }
}
/*function _jsonRep(req, res, next) {
    let object = store.select(req.params.collection, req.params.id)
    if (Array.isArray(object)) {
        if (req.params.collection == "tweets") {
            let reqUrl = req.protocol + '://' + req.get('host') + req.originalUrl
            object = { items: object, "href": reqUrl }
            object.items.forEach(item => {
                const itemUrl = reqUrl + '/' + item.id
                item.href = itemUrl
                Object.entries(dependencies[req.params.collection]).forEach((collection) => {
                    item[collection[1]].href = req.protocol + '://' + req.get('host') + '/' + collection[0] + '/' + item[collection[1]].id
                })
            })
        } else if (req.params.collection == "users") {
            let reqUrl = req.protocol + '://' + req.get('host') + req.originalUrl
            object = { items: object, "href": reqUrl }
            object.items.forEach(item => {
                const itemUrl = reqUrl + '/' + item.id
                item.href = itemUrl
            })
        }
    } else {
        if (req.params.collection == "tweets") {
            console.log(object)
            object.href = req.protocol + '://' + req.get('host') + req.originalUrl + ', ' + req.protocol + '://' + req.get('host') + '/' + "users" + '/' + object.user.id
        } else {
            object.href = req.protocol + '://' + req.get('host') + req.originalUrl
        }
    } if (object == null) {
        next(new HttpError[404]);
    } else {
        return object;
    }
};
*/
store.insert("users", { "firstname": "Quentin", "lastname": "Entenhausen" })

store.insert("users", { "firstname": "Franz", "lastname": "List"})
store.insert("tweets", { "user": "Kafka", "date": "01.01.01" })

app.route('/users')
    .get( (req,res,next)=> {
        console.log("Die Route users wurde mit der Methode GET aufgerufen")
        let users = store.select("users")
        res.contentType('application/json').send(users)
       // res.status(200).contentType('application/json').send(_jsonRep(req, res))
    })
app.route('/users/:id')
   .get( (req, res) => {
        console.log("Der Pfad users wurde mit der ID: " + req.params.id + " aufgerufen")
        
        let user = store.select("users", req.params.id)
        if (user==null){    
            res.status(404).contentType('text/plain').send('Sorry cant find that USER_ID!'+ httpErr[406]);
        }
        res.contentType('application/json').send (user )
    })
 app.route('/users/:id')
   .put( (req, res) => {
        console.log("Der Pfad users wurde mit der ID: " + req.params.id + " aufgerufen to")
        let user = store.select("users", req.params.id)
        if (user==null){
            res.status(404).contentType('text/plain').send('Sorry cant find this USER_ID!');
        }

       // console.log(user.id)
        //console.log(req.params.id)
        //console.log("users", req.params.id)
        let updatedBody = req.body
        store.replace("users",  req.params.id, updatedBody)
        
        res.contentType('application/json').send (store.select("users") )
})


app.route('/users/:id')
         .delete ((req, res,next) =>{
        console.log("Der Pfad users wurde mit der ID: " + req.params.id + " aufgerufen und wird geloescht")
        let user = store.select("users", req.params.id)
        console.log(user)
        if (user==null){
            res.status(404).contentType('text/plain').send('Sorry cant find that USER_ID!');
        }
        store.remove ("users", req.params.id)
        res.send('deleted')
        })
      

app.route('/tweets')
    .get( (req,res,next)=> {
          console.log("Die Route tweets wurde mit der Methode GET aufgerufen")
          let tweets = store.select("tweets")
          var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
          res.contentType('application/json').send(tweets)
          
    })
    
// creates a new object in the 'db' returns the new object

app.route('/tweets')    
    .post((req,res) => {
        if(!(req.accepts('json'))) {
            next(new httpError[406])
        }
          console.log("Die Route fuer tweets wurde mit der Methode POST aufgerufen. ")
          let body = req.body
        if (body == null) {
            next(new HttpError[404]);
        }    
          store.insert("tweets", body)
         res.contentType('application/json').send(store.select("tweets"))

    })
app.route('/tweets/:id')
    .get( (req, res) => {
            console.log("Der Pfad tweets wurde mit der ID: " + req.params.id + " aufgerufen")
            let tweet = store.select("tweets", req.params.id)
            if (tweet==null){
                res.status(404).contentType('text/plain').send('Sorry cant find that USER_ID!');
            }
    
            res.contentType('application/json').send (tweet)
    })
app.route('/tweets/:id')     
    .delete ((req, res,next) =>{
        console.log("Der Pfad tweets wurde mit der ID: " + req.params.id + " aufgerufen und wird geloescht")
        let tweet = store.select("tweets", req.params.id)
        console.log(tweet)
        if (tweet==null){
            res.status(404).contentType('text/plain').send('Sorry cant find that USER_ID!');
        }

        store.remove ("tweets", req.params.id)
        res.send('deleted')
       
        })

  // function sends static file, that asked from Folder 'public'
app.use( express.static('public'));

app.use(function(req, res, next) {
          res.status(404).contentType('text/plain').send('Sorry cant find that!');
});


//let newAuthor=req.body 
app.listen(3000, function() { 
  console.log('Example app listening on port 3000!');
});
